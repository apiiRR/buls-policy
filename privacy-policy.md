## Privacy Policy for Be Buls

Last Updated: 27 February 2024

We at Be Buls value the privacy of our users. This Privacy Policy explains how we collect, use, and protect the personal information you provide when using our application and related services. By using our application, you agree to the collection and use of information as described in this policy.

### Information We Collect

We may collect personal information that you directly provide, such as your name, email address, phone number, address, and payment information when you use our payment services through Midtrans. Additionally, we may also collect non-personal information such as statistical data about app usage, activity logs, and technical information automatically collected when you access or use our application.

### Use of Information

The information we collect may be used to:

Provide the services and features you request.
Process your payment transactions and manage your account.
Send confirmations, updates, and notifications related to our services.
Improve, develop, and personalize our application.
Analyze usage trends and behaviors to enhance user experience and our services.
Protect our rights, property, or safety, as well as that of our users.
Comply with applicable laws and court orders.
How We Share Information

We will not sell, rent, or trade your personal information to third parties without your consent, except as described in this policy or as required by law.

We may share your personal information with third-party service providers who assist us in providing services to you, such as payment service providers like Midtrans. These service providers are only permitted to use such information as instructed by us and in the context of the services they provide.

### Maintenance and Security of Information

We implement appropriate physical, electronic, and procedural security measures to protect your personal information from unauthorized access, use, or disclosure. However, no method of transmission or storage of data is 100% secure. Therefore, we cannot guarantee the security of information you provide online or through our application.

### Your Choices

You may choose not to provide certain personal information to us, but this may result in you being unable to use some features or services of our application.

### Changes to the Privacy Policy

We may update this Privacy Policy from time to time by posting a revised version in our application. Such changes will be effective immediately upon posting. We encourage you to check our Privacy Policy periodically to stay up-to-date on our practices.

### Contact Us

If you have any questions about our Privacy Policy or how we manage personal information, please contact us at itdivisi.berdikari@gmail.com.
